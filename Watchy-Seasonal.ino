#include <Watchy.h>  //include the Watchy library
#include "Nunito_ExtraBold38pt7b.h"
#include "Nunito_Bold12pt7b.h"
#include "settings.h"
#include "images.h"


class WatchFace : public Watchy {  //inherit and extend Watchy class
public:
    WatchFace(const watchySettings& s)
    : Watchy(s) {}
    void drawWatchFace() {  //override this method to customize how the watch face looks

        int16_t x1, y1;
        uint16_t w, h;
        String textstring;

        //drawbg
        display.fillScreen(GxEPD_WHITE);

        display.setTextColor(GxEPD_BLACK);
        display.setTextWrap(false);

        //draw seasonal backgrounds
        switch (currentTime.Month) {
            case 4:
                display.drawBitmap(0, 0, easter, 200, 200, GxEPD_BLACK);
                break;
            case 10:
                display.drawBitmap(0, 0, halloween, 200, 200, GxEPD_BLACK);
                break;
            case 12:
                if (currentTime.Day == 31)
                    display.drawBitmap(0, 0, newyears, 200, 200, GxEPD_BLACK);
                else
                    display.drawBitmap(0, 0, christmas, 200, 200, GxEPD_BLACK);
        }

        //draw steps
        display.setFont(&Nunito_Bold12pt7b);
        if (currentTime.Hour == 0 && currentTime.Minute == 0) {
            sensor.resetStepCounter();
        }
        uint32_t stepCount = sensor.getCounter();
        textstring = stepCount;
        textstring += " steps today";
        display.getTextBounds(textstring, 0, 0, &x1, &y1, &w, &h);
        display.setCursor(100 - w / 2, 72 + h);
        display.print(textstring);

        //draw time
        display.setFont(&Nunito_ExtraBold38pt7b);

        textstring = currentTime.Hour;
        textstring += ":";
        if (currentTime.Minute < 10) {
            textstring += "0";
        }
        textstring += currentTime.Minute;
        display.getTextBounds(textstring, 0, 0, &x1, &y1, &w, &h);
        display.setCursor(98 - w / 2, 160);
        display.print(textstring);


        //draw date
        display.setFont(&Nunito_Bold12pt7b);
        textstring = currentTime.Day;
        textstring += ".";
        textstring += currentTime.Month;
        textstring += ".";

        textstring += currentTime.Year + 1970;
        display.getTextBounds(textstring, 0, 0, &x1, &y1, &w, &h);
        display.setCursor(100 - w / 2, 170 + h);
        display.print(textstring);

        //draw battery
        float batt = (getBatteryVoltage() - 3.3) / 0.82;
        if (batt > 0) display.fillRect(4, 196, 192 * batt, 4, GxEPD_BLACK);
    }
};

WatchFace m(settings);  //instantiate your watchface

void setup() {
    m.init();  //call init in setup
}

void loop() {
    // this should never run, Watchy deep sleeps after init();
}
